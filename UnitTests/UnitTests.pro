QT += testlib
QT += widgets

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_testsute.cpp

# Include the files of tested class to be able to compile it separately for the test purpose
INCLUDEPATH += ../MSWestfalia

HEADERS += ../MSWestfalia/mainwindow.h \
           ../MSWestfalia/libs/randombytes.h \
           ../MSWestfalia/libs/tweetnacl.h

SOURCES += ../MSWestfalia/mainwindow.cpp \
           ../MSWestfalia/libs/randombytes.c \
           ../MSWestfalia/libs/tweetnacl.c

