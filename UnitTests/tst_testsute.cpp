#include <QtTest>

// Define TEST_ENABLED to make accessible class' private
#define TEST_ENABLED

// Include mainwindow cpp to be able to test the main class
#include "mainwindow.cpp"

class testSute : public QObject
{
    Q_OBJECT

private:
    void fill_str_and_trigger(MainWindow *w, QString test_str);

public:
    testSute();
    ~testSute();

private slots:
    void main_encrypt_decrypt_scenario_good_str();
    void main_encrypt_decrypt_scenario_bad_str();
    void check_hexes();
};

testSute::testSute()
{
}

testSute::~testSute()
{
}

void testSute::main_encrypt_decrypt_scenario_good_str()
{
    // This test puts test string to needed field, triggers encryption
    // and then checks if the result is expected.
    MainWindow w;
    QString test_str = "This is test string";
    fill_str_and_trigger(&w, test_str);

    // Veryfy
    QCOMPARE(w.ui->lineDecrypted->text(), test_str);
}

void testSute::main_encrypt_decrypt_scenario_bad_str()
{
    // This test puts wrong test string (more than 20 symbols) to needed field,
    // triggers encryption and then checks if the result is expected.
    MainWindow w;
    QString test_str = "This is test string that consists more than 20 symbols";
    fill_str_and_trigger(&w, test_str);

    // Then check if the result if expectec failed
    QEXPECT_FAIL("", "Expected fail of test, that is good test result", Continue);
    QCOMPARE(w.ui->lineDecrypted->text(), test_str);

    // Fix the test input and try again
    test_str.resize(20);
    QCOMPARE(w.ui->lineDecrypted->text(), test_str);
}

void testSute::check_hexes()
{
    // This test checks if all the hexes are filled in case of good sting
    MainWindow w;
    QString test_str = "This is test string";
    fill_str_and_trigger(&w, test_str);

    QCOMPARE(w.ui->linePublicKey->text().isEmpty(), false);
    QCOMPARE(w.ui->linePrivateKey->text().isEmpty(), false);
    QCOMPARE(w.ui->lineEncryptedHex->text().isEmpty(), false);
}

void testSute::fill_str_and_trigger(MainWindow *w, QString test_str)
{
    // Fill input field
    QTest::keyClicks(w->ui->lineToEncrypt, test_str);

    // Trigger encryption
    QTest::mouseClick(w->ui->encryptButton, Qt::MouseButton::LeftButton);
}

QTEST_MAIN(testSute)

#include "tst_testsute.moc"
