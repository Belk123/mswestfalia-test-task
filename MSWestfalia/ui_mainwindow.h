/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *lineToEncrypt;
    QPushButton *encryptButton;
    QSpacerItem *verticalSpacer;
    QLabel *label;
    QLineEdit *linePublicKey;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_2;
    QLineEdit *linePrivateKey;
    QSpacerItem *verticalSpacer_3;
    QLabel *label_3;
    QLineEdit *lineEncryptedHex;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_4;
    QLineEdit *lineDecrypted;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(647, 363);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        lineToEncrypt = new QLineEdit(centralwidget);
        lineToEncrypt->setObjectName(QStringLiteral("lineToEncrypt"));
        lineToEncrypt->setMaxLength(20);

        verticalLayout_5->addWidget(lineToEncrypt);

        encryptButton = new QPushButton(centralwidget);
        encryptButton->setObjectName(QStringLiteral("encryptButton"));

        verticalLayout_5->addWidget(encryptButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);

        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_5->addWidget(label);

        linePublicKey = new QLineEdit(centralwidget);
        linePublicKey->setObjectName(QStringLiteral("linePublicKey"));
        linePublicKey->setEnabled(true);
        linePublicKey->setEchoMode(QLineEdit::Normal);
        linePublicKey->setReadOnly(true);

        verticalLayout_5->addWidget(linePublicKey);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_2);

        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_5->addWidget(label_2);

        linePrivateKey = new QLineEdit(centralwidget);
        linePrivateKey->setObjectName(QStringLiteral("linePrivateKey"));
        linePrivateKey->setReadOnly(true);

        verticalLayout_5->addWidget(linePrivateKey);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_3);

        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_5->addWidget(label_3);

        lineEncryptedHex = new QLineEdit(centralwidget);
        lineEncryptedHex->setObjectName(QStringLiteral("lineEncryptedHex"));
        lineEncryptedHex->setReadOnly(true);

        verticalLayout_5->addWidget(lineEncryptedHex);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_4);

        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_5->addWidget(label_4);

        lineDecrypted = new QLineEdit(centralwidget);
        lineDecrypted->setObjectName(QStringLiteral("lineDecrypted"));
        lineDecrypted->setReadOnly(true);

        verticalLayout_5->addWidget(lineDecrypted);


        horizontalLayout->addLayout(verticalLayout_5);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MS Westfalia QT test by Nekrasov", Q_NULLPTR));
        lineToEncrypt->setPlaceholderText(QApplication::translate("MainWindow", "Type a message to encrypt", Q_NULLPTR));
        encryptButton->setText(QApplication::translate("MainWindow", "Encrypt", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Public key", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Private key", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Encrypted message dump", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Decrypted message", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
