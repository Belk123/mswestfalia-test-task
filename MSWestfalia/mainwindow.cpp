#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_encryptButton_clicked()
{
    // Read the message from the mesage field
    QString message_qs = ui->lineToEncrypt->text();

    // Check if the message exists
    if (message_qs.isEmpty()){
        QMessageBox::warning(this,
            "Warning",
            "The message field must contain a message to encrypt"
        );
        return;
    }

    ui->lineDecrypted->setText(encrypt_decrypt(message_qs));
}

QString MainWindow::encrypt_decrypt(QString message_qs, bool dump_hex_b)
{
    // Generate key pair and dump them
    u8 public_key_u8[crypto_box_PUBLICKEYBYTES];
    u8 secret_key_u8[crypto_box_SECRETKEYBYTES];
    crypto_box_keypair(public_key_u8, secret_key_u8);

    // Generate nonce
    u8 nonce_u8[crypto_box_NONCEBYTES];
    randombytes(nonce_u8, crypto_box_NONCEBYTES);

    // Encryption section

    // From NaCl doc:
    // WARNING: Messages in the C NaCl API are 0-padded versions of messages
    // in the C++ NaCl API. Specifically: The caller must ensure, before calling
    // the C NaCl crypto_box function, that the first crypto_box_ZEROBYTES bytes
    // of the message m are all 0. Typical higher-level applications will work with
    // the remaining bytes of the message; note, however, that mlen counts all of the
    // bytes, including the bytes required to be 0.

    // Extend message len with padding size
    int message_len_int = crypto_box_ZEROBYTES + message_qs.size();

    // Encrypt the message. rightJustified does needed padding
    u8 encrypted_message_u8[message_len_int];
    crypto_box(encrypted_message_u8,
               (u8*)message_qs.rightJustified(message_len_int, 0).toLocal8Bit().data(),
               message_len_int, nonce_u8, public_key_u8, secret_key_u8);

    // Decryption section

    // The buffer size should be message_len + 1 to be able to contain
    // final \0 to get the correct string
    char decrypted_message_char[message_len_int + 1];

    crypto_box_open((u8*)decrypted_message_char, encrypted_message_u8,
                    message_len_int, nonce_u8, public_key_u8, secret_key_u8);

    decrypted_message_char[message_len_int] = '\0';

    if (dump_hex_b) {
        hexdump(ui->linePublicKey, public_key_u8, crypto_box_PUBLICKEYBYTES);
        hexdump(ui->linePrivateKey, secret_key_u8, crypto_box_SECRETKEYBYTES);
        hexdump(ui->lineEncryptedHex, encrypted_message_u8, message_len_int);
    }

    // Decrypted message has the same padding with size = crypto_box_ZEROBYTES
    return QString::fromLocal8Bit(decrypted_message_char + crypto_box_ZEROBYTES);
}

void MainWindow::hexdump(QLineEdit *qline_p, u8 *val_u8_p, int len_int)
{
    QString hex_str_qs = "";
    for (int i = 0; i < len_int; i++) {
        hex_str_qs += QChar(val_u8_p[i]);
    }
    qline_p->setText(hex_str_qs.toLatin1().toHex());
}
