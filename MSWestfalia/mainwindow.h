#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Define private access method depending on build configuration
// We should be able to call private methods while testing
#ifndef TEST_ENABLED
#define PRIVATE_ACCESS private
#else
#define PRIVATE_ACCESS public
#endif

// Include C libs
extern "C" {
    #include "libs/randombytes.h"
    #include "libs/tweetnacl.h"
}

// Inclue QT libs
#include <QString>
#include <QLineEdit>
#include <QMessageBox>

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

typedef unsigned char u8;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_encryptButton_clicked();

// Kindly check PRIVATE_ACCESS defenition with comments
PRIVATE_ACCESS:
    Ui::MainWindow *ui;
    void hexdump(QLineEdit *qline, u8 *val, int len);
    QString encrypt_decrypt(QString message, bool print_hex = true);
};
#endif // MAINWINDOW_H
